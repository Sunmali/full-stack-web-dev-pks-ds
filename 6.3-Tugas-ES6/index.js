// Soal 1
console.log("Soal 1")
const persegiPanjang = () => {
        let panjang = 12
        let lebar = 10

        let luas = panjang * lebar;
        let keliling = 2 * (panjang + lebar);
        
        const data = {luas, keliling}

        console.log(data);
}
persegiPanjang()
console.log(" ")


// Soal 2
console.log("Soal 2")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}

newFunction("William", "Imoh").fullName() 
console.log(" ")


// Soal 3
console.log("Soal 3")
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)
console.log(" ")


// Soal 4
console.log("Soal 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west,...east]
console.log(combined)
console.log(" ")


// Soal 5
console.log("Soal 5")
const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)
console.log(" ")