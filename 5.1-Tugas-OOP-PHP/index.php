<?php 

// Parent Class
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    
    public function atraksi($nama, $keahlian) {
        $this->nama = $nama;
        $this->keahlian = $keahlian;

        return $this->nama ." Sedang ". $this->keahlian;
    }
}

trait Fight {
    public $attackPower;
    public $defensePower;

    public function serang($hewan1, $hewan2) {
        $this->hewan1 = $hewan1;
        $this->hewan2 = $hewan2;

        return $this->hewan1 ." sedang menyerang ". $this->hewan2;
    }

    public function diserang($hewan2, $hewan1) {
        // gk ngerti 
        // coach yang function 
        // ini gimana caranya:(
        $this->hewan1 = $hewan1;
        $this->hewan2 = $hewan2;

        return $this->hewan2 ." sedang diserang ". $this->hewan1 ."</br>";

    }
}

// Child Classes
class Elang {
    use Hewan, Fight;
    public function getInfoHewan(){
        $this->nama = "Elang";
        $this->jumlahKaki = "2";
        $this->keahlian = "Terbang tinggi";
        $this->attackPowerElang = 10;
        $this->defensePowerElang = 5;

        return "Nama Hewan: ". $this->nama ."</br>
                Total Darah: ". $this->darah ."</br>
                Jumlah Kaki: ". $this->jumlahKaki ."</br>
                Keahlian: ". $this->keahlian ."</br>
                Attack Power: ". $this->attackPowerElang ."</br>
                Defense Power: ". $this->defensePowerElang ."</br></br>";
    }

}

class Harimau {
    use Hewan, Fight;
    public function getInfoHewan(){
        $this->nama = "Harimau";
        $this->jumlahKaki = "4";
        $this->keahlian = "Berlari cepat";
        $this->attackPowerHarimau = 7;
        $this->defensePowerHarimau = 8;

        return "Nama Hewan: ". $this->nama ."</br>
                Total Darah: ". $this->darah ."</br>
                Jumlah Kaki: ". $this->jumlahKaki ."</br>
                Keahlian: ". $this->keahlian ."</br>
                Attack Power: ". $this->attackPowerHarimau ."</br>
                Defense Power: ". $this->defensePowerHarimau ."</br></br>";
    }
}

$elang = new Elang();
    echo "<h2>Elang</br>getInfoHewan()</h2>". $elang->getInfoHewan();
    echo "<b>atraksi()</br></b>". $elang->atraksi("Elang", "Terbang tinggi"). "</br></br>";
    echo "<b>serang()</br></b>". $elang->serang("Elang","Harimau")."</br></br>";
    echo "<b>diSerang()</br></b>". $elang->diserang("Elang","Harimau") ;

$harimau = new Harimau();
    echo "<h2>Harimau</br>getInfoHewan()</h2>". $elang->getInfoHewan();
    echo "<b>atraksi()</br></b>". $harimau->atraksi("Harimau", "Berlari cepat"). "</br></br>";
    echo "<b>serang()</br></b>". $elang->serang("Elang","Harimau")."</br></br>";
    echo "<b>diSerang()</br></b>". $elang->diserang("Harimau","Elang") ;
?>
