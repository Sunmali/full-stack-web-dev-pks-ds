<?php

namespace App\Mail;

use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Tymon\JWTAuth\Claims\Subject;

class PostAuthorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $comments;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comment.post_author_mail')
                    ->Subject('Full Stack Web Dev PKS DS');
                    // ->with
    }
}
