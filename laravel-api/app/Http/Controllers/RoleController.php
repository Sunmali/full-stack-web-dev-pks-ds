<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->get();

        if($roles){
            return response()->json([
                'success' => true,
                'message' => 'Lihat data Role berhasil',
                'data' => $roles
            ]);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Lihat data Role gagal',
        ], 409);  
        
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $roles =  Role::create([
            'name' => $request->name,
        ]);

        if($roles){
            return response()->json([
                'success' => true,
                'message' => 'Masukkan data Role berhasil',
                'data' => $roles,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Masukkan data Role gagal',
        ], 409);  
    }

    public function show($id)
    {
        $roles = Role::find($id);

        if($roles){
            return response()->json([
                'success'   => true,
                'message'   => 'Tampilkan data Role dengan id= '. $id .' berhasil',
                'data'      => $roles,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Tampilkan data Role dengan id= '. $id .' gagal',
            'data'      => $roles,
        ], 404);
        
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $roles = Role::find($id);

        if($roles){
            $roles->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Data Role dengan id= '. $id .' berhasil di update',
                'data'      => $roles,
            ]);
        }
        
        return response()->json([
            'success'   => false,
            'message'   => 'Data Role dengan id= '. $id .' tidak di temukan',
            'data'      => $roles,
        ], 404);
    }

    public function destroy($id)
    {
        $roles = Role::find($id);

        if($roles){

            $roles->delete();

            return response()->json([
                'success'   => true,
                'message'   => 'Data Role berhasil di delete',
                'data'      => $roles,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Role gagal di hapus',
            'data'      => $roles,
        ], 404);
    }
}

