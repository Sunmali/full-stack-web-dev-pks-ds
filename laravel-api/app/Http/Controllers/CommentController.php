<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store','update','destroy');
    }
    
    public function index()
    {
        $comments = Comment::latest()->get();

        if($comments){
            return response()->json([
                'success' => true,
                'message' => 'Lihat data Comment berhasil',
                'data' => $comments
            ]);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Lihat data Comment gagal',
        ], 409);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments =  Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);


        // Memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comments));

        
        if($comments){
            return response()->json([
                'success' => true,
                'message' => 'Masukkan data Comment berhasil',
                'data' => $comments,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message' => 'Masukkan data Comment gagal',
        ], 409);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::find($id);

        if($comments){
            return response()->json([
                'success'   => true,
                'message'   => 'Tampilkan Comment berhasil',
                'data'      => $comments,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Tampilkan Comment gagal',
            'data'      => $comments,
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'content' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comment::find($id);

        if($comments){
            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Data post bukan milik user yang sedang login',
                ], 403);
            }

            $comments->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Data dengan id '. $id .' berhasil di update',
                'data'      => $comments,
            ]);
        }
        
        // return response()->json([
        //     'success'   => false,
        //     'message'   => 'Data dengan id '. $id .' tidak di temukan',
        //     'data'      => $comments,
        // ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comment::find($id);

        if($comments){

            return response()->json([
                'success'   => false,
                'message'   => 'Data post bukan milik user yang sedang login',
            ], 403);

            $comments->delete();

            return response()->json([
                'success'   => true,
                'message'   => 'Data post berhasil di delete',
                'data'      => $comments,
            ], 200);
        }

        // return response()->json([
        //     'success'   => false,
        //     'message'   => 'Data post gagal di hapus',
        //     'data'      => $comments,
        // ], 404);
    }
}
