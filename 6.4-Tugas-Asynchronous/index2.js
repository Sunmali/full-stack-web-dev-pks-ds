// Soal Nomor 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

// Jawaban 2
readBooksPromise(10000,books[0])
.then(function(sisaWaktu){
    readBooksPromise(sisaWaktu,books[1])
    .then(function(sisaWaktu){
        readBooksPromise(sisaWaktu,books[2])
        .then(function(sisaWaktu){
            readBooksPromise(sisaWaktu,books[3])
            .then(function(sisaWaktu){
                // console.log(sisaWaktu)
            })
            .catch(error => console.log(error))
        })
        .catch(error => console.log(error))
    })
    .catch(error => console.log(error))
})
.catch(error => console.log(error))

