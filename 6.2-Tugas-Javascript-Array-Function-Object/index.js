// Soal 1
console.log("##############")
console.log("### Soal 1 ###")
console.log("##############")

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort().forEach(function(hewan){
    console.log(hewan)
})

console.log(" ")


// Soal 2
console.log("##############")
console.log("### Soal 2 ###")
console.log("##############")

function introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}` 
}
 
var data = {
    name : "John" , 
    age : 30 , 
    address :  "Jalan Pelesiran" , 
    hobby : "Gaming"
}

 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

console.log(" ")


// Soal 3
console.log("##############")
console.log("### Soal 3 ###")
console.log("##############")

function hitung_huruf_vokal(string){
    var vokal = "aiueoAIUEO"
    var count = 0

    for (var x = 0; x < string.length; x++) {
        if (vokal.indexOf(string[x]) !== -1) {
            count++
        }
    }
    return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2

console.log(" ")

// Soal 4
console.log("##############")
console.log("### Soal 4 ###")
console.log("##############")

function hitung(number) {
    return (2+((number -2)*2));
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

console.log(" ")